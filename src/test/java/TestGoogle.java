import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;

public class TestGoogle {

    @Parameters("word")
    @Test(description = "Verify search for the word Apple")
    public void verifySearchEngine(@Optional("Apple") String word) {

        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");

        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys(word);
        searchInput.submit();
        Assert.assertTrue(driver.getTitle().contains(word), "Title does not contain: " + word);

        WebElement imagesButton = driver.findElement(By.xpath("//div[@class='hdtb-mitem hdtb-imb'][1]/a"));
        imagesButton.click();

        WebElement imagesTag = driver.findElement(By.id("islmp"));
        Assert.assertTrue(imagesTag.isDisplayed());
        File scrFile = imagesTag.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("screenshot\\imagesTab.png"));
            System.out.println("Screenshot captured successfully");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        driver.quit();
    }
}

